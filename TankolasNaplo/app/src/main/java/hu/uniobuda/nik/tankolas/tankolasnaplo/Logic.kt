package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.content.Context
import android.graphics.Bitmap

class Logic(context: Context) {


    var Cars = ArrayList<Car>()
    private var dataBase = DataBaseHandler(context)

    //region For logic methods
    //Car object insert to database
    @Suppress("FunctionName")
    fun AddCarToData(car: Car) {

        dataBase.insertCarData(car)
        ReadCarsData()
    }
    fun CarPictureSet(picname: String, picture: Bitmap) :String{
        return dataBase.CarPictureSet(picname, picture)
    }

    //Entry object insert to database
    @Suppress("FunctionName")
    fun AddToEntryData(entry: Entry, car: Car) {

        dataBase.insertEntryData(entry, car)  //Car: because of the foreign key
        car.entryList.add(entry)
        dataBase.upDateKms(car.Nickname, entry.km) //Updating the KM in the picked car item
    }

    //Get a car object by it's nickname (ID in database)
    @Suppress("FunctionName")
    fun SearchCarByNick(nick: String): Car {
        ReadCarsData()
        var foundCar: Car = Cars[0]
        for (car in Cars) {
            if (car.Nickname == nick) {
                foundCar = car
            }
        }
        return foundCar
    }

    //For updates
    @Suppress("FunctionName")
    fun ReadCarsData() {

        Cars = dataBase.readCarsData()

        for (car in Cars) {
            car.entryList = dataBase.readEntryData(car.Nickname)
        }
    }

    //Read all Entry object from database
    fun readAllEntry(): ArrayList<Entry> {

        return dataBase.readAllEntryData()
    }

    fun readCarNames(): ArrayList<String> {
        var carNickNames = ArrayList<String>()
        for (car in Cars) {
            carNickNames.add(car.Nickname)
        }
        return carNickNames
    }

    fun searchEntry(carName: String, km: Int): Entry {
        var entries = readAllEntry()
        var retEntry = entries[0]
        for (entry in entries) {
            if (entry.carNick == carName && entry.km == km) {
                retEntry = entry
            }
        }
        return retEntry
    }

    fun readSelectedEntry(cars: ArrayList<String>): ArrayList<Entry> {


        var selectedEntries = ArrayList<Entry>()
        for (car in cars) {
            var onCarsEntries = dataBase.readAllEntriesByCar(car)
            for (entry in onCarsEntries) {
                selectedEntries.add(entry)
            }

        }
        return selectedEntries
    }

    //delete car
    fun DeleteCar(car: Car) {
        dataBase.deleteAllEntryForDeleteCar(car)
        dataBase.deleteCarItem(car)
    }
//endregion

    //region For statistics
    //region At all statistics
    fun AverageConsumptionAtAll(thisCar: Car): Double {

        var consumpion = 0
        var kms = thisCar.actualKM
        var allkms = kms - thisCar.kmfirst
        for (entry in thisCar.entryList) {
            consumpion += entry.liter
        }

        var average = (consumpion.toDouble() / allkms.toDouble()) * 100  //X Liter / 100KM
        return average
    }

    fun AverageCostPerKmAtAll(thisCar: Car): Double {
        var cost = 0
        for (entry in thisCar.entryList) {
            cost += entry.totalCost
        }
        return (cost / (thisCar.actualKM - thisCar.kmfirst)).toDouble()
    }

    fun MostExpensiveEntry(entries: ArrayList<Entry>): Entry {

        var ExpensiveEntry = entries[0]
        for (entry in entries) {
            if (entry.totalCost > ExpensiveEntry.totalCost) {
                ExpensiveEntry = entry
            }
        }
        return ExpensiveEntry
    }

    fun AllCost(thisCar: Car): Int {
        var allCost = 0
        for (entry in thisCar.entryList) {
            allCost += entry.totalCost
        }
        return allCost
    }


    //region For one Entry statistics
    //return how much km was drive in this session
    fun KmsInThisSession(car: Car, thisentry: Entry): Int {
        var pointer = 0
        ReadCarsData()
        for ((counter, entry) in car.entryList.withIndex()) {
            if (entry.km == thisentry.km) {
                pointer = counter - 1
            }
        }
        return if (pointer == -1) {
            thisentry.km - car.kmfirst
        } else {
            thisentry.km - car.entryList[pointer].km
        }
    }

//endregion
//endregion


}