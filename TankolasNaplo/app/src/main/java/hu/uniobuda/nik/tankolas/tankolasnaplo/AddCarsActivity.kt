package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_add_cars.*


class AddCarsActivity : AppCompatActivity() {


    private var carNames = ArrayList<String>()
    private lateinit var option: Spinner
    private lateinit var logic: Logic
    val context = this
    private var pictureId = R.drawable.car
    private lateinit var image: CircleImageView
    private var filePath = "default"


    private val IMAGE_PICK_CODE = 1000
    private val PERMISSION_CODE = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.titleOneCar)
        setContentView(R.layout.activity_add_cars)

        //region Startings

        image = findViewById(R.id.circle_auto)
        image.setImageResource(pictureId)


        logic = Logic(context)
        logic.ReadCarsData()
        carNames = logic.readCarNames()
        val options = arrayOf(FuelType.Petrol.toString(), FuelType.Electricity.toString(), FuelType.Diesel.toString(), FuelType.Gas.toString())
        this.option = findViewById(R.id.fluel_spinner)
        this.option.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options)
        //endregion
        circle_auto.setOnClickListener {
            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED) {
                    //permission denied
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)

                } else {
                    //permission already granted
                    pickImageFromGallery();
                }
            } else {
                // system OS is < Marsmallow
                pickImageFromGallery();
            }
        }

        saveEntry.setOnClickListener {
            if (nickname_text.text.toString().isNotEmpty() &&
                    brand_text.text.toString().isNotEmpty() &&
                    model_text.text.toString().isNotEmpty() &&
                    plate_text.text.toString().isNotEmpty() &&
                    fluel_spinner.selectedItem != null &&
                    km_text.text.toString().isNotEmpty()) {


                if (isCarNameValid(nickname_text.text.toString()) &&
                        isPlateValid(plate_text.text.toString())) {

                    var car = Car(nickname_text.text.toString(),
                            brand_text.text.toString(),
                            model_text.text.toString(),
                            plate_text.text.toString().toUpperCase(),
                            fluel_spinner.selectedItem.toString(),
                            km_text.text.toString().toInt(),
                            km_text.text.toString().toInt(),
                            filePath)

                    logic = Logic(context)
                    logic.AddCarToData(car)
                    Toast.makeText(context, R.string.carAddedToDataBase, Toast.LENGTH_SHORT).show()
                    val intent = Intent(context, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            } else {
                Toast.makeText(context, R.string.fillEverything, Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("ResourceType")

    private fun pickImageFromGallery() {
        //intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    //permission from popup denied
                    Toast.makeText(this, R.string.permissionDenied, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            val imageUri = data?.data
            circle_auto.setImageURI(imageUri)
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
            var carPicName = ""
            //Hogy tuti egyedi nevet kapjon a fájl
            if (logic.Cars.isNotEmpty()) {

                if (lastCarWithPic() != -1) {
                    var lastpiece = logic.Cars[lastCarWithPic()].image.split('/')
                    var getnumber = lastpiece.last().split('.')
                    var number = getnumber[0].toInt()
                    carPicName = (number + 1).toString()
                } else {
                    carPicName = 0.toString()
                }
            } else {
                carPicName = 0.toString()
            }
            var newpath = logic.CarPictureSet(carPicName, bitmap)
            Toast.makeText(context, newpath, Toast.LENGTH_LONG).show()
            filePath = newpath

        }
    }

    fun lastCarWithPic(): Int {
        var lastcar = -1
        for ((idx, car) in logic.Cars.withIndex()) {
            if (car.image != "default") {
                lastcar = idx
            }
        }
        return lastcar
    }


    //region Validator functions
    private fun isCarNameValid(name: String): Boolean {


        for (names in carNames) {
            if (names == name) {
                Toast.makeText(context, R.string.carAlreadyExists, Toast.LENGTH_SHORT).show()
                return false
            }
        }
        return true
    }

    private fun isPlateValid(plate: String): Boolean {
        if (plate.length == 7) {
            if (plate.contains('-')) {
                var platestring = plate.split('-')
                for (char in platestring[0]) {
                    if (!char.isLetter()) {
                        Toast.makeText(context, R.string.plateOnlyLetters, Toast.LENGTH_SHORT).show()
                        return false

                    }

                }
                for (char in platestring[1]) {
                    if (!char.isDigit()) {
                        Toast.makeText(context, R.string.plateOnlyNumbers, Toast.LENGTH_SHORT).show()
                        return false
                    }
                }
            } else {
                Toast.makeText(context, R.string.plateFormat, Toast.LENGTH_LONG).show()
                return false

            }
        } else {
            Toast.makeText(context, "A rendszámnak 6 karakterből( és egy kötőjelből) kell állnia!", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }


//endregion


}
