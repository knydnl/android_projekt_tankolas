package hu.uniobuda.nik.tankolas.tankolasnaplo


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_mycars.*

class MyCarsFragment: Fragment( ){


    private var mycars = ArrayList<Car>()
    private lateinit var mylogic : Logic
    private var adapter : CarsAdapter? = null


    companion object {
        fun newInstance(logic: Logic): MyCarsFragment{

            val fragment = MyCarsFragment()
            fragment.mylogic=logic
            fragment.mylogic.ReadCarsData()
            fragment.mycars=logic.Cars
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.mylogic.ReadCarsData()
        this.mycars=mylogic.Cars
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(container?.context).inflate(R.layout.fragment_mycars, container, false)
    }

    override fun onResume() {
        super.onResume()
        this.mylogic.ReadCarsData()
        this.mycars=mylogic.Cars
        adapter=null
        updateUI()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addcar.setOnClickListener{
            val intent= Intent(this.context, AddCarsActivity::class.java)

            startActivity(intent)
        }

        updateUI()
    }
    private fun updateUI(){
        if(adapter==null){
            adapter= CarsAdapter(requireContext(),mycars)
            car_recyclerview.adapter=adapter
            car_recyclerview.layoutManager= LinearLayoutManager(this.context)
        }
        adapter?.notifyDataSetChanged()
    }
}