package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.nfc.Tag
import android.util.Log
import android.widget.Toast
import java.io.File
import java.io.FileOutputStream

//region DataBaseNames
val DATABASE_NAME = "ALL_DATA"

//Az autók rögzítéséhez
val TABLE_NAME_1 = "Car_table"
val COL_ID = "Nickname"  //Car ID == Nickname
val COL_BRAND = "BrandName"
val COL_MODEL = "Model"
val COL_PLATE = "Plate_number"
val COL_FUEL = "Fuel_type"
val COL_AC_KM = "Actual_Kilometers"
val COL_KM = "Kilometers"
val COL_IMG = "Car_image_name"

//Az egyes tankolások rögzítéséhez
val TABLE_NAME_2 = "Entry_table"
val COL_ID_PRI = "id"
val COL_ID2 = "FK_car_nickname"
val COL_DATE = "DATE"
val COL_LITER = "LITERS"
val COL_KMS = "New_KMs"
val COL_COST = "FT_Per_Liter"
//endregion


class DataBaseHandler(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {

    //Ha az eszközön nincsen még adatbázis, létrehozza.
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE " + TABLE_NAME_1 + " (" +
                COL_ID + " VARCHAR(50) PRIMARY KEY," +
                COL_BRAND + " VARCHAR(50)," +
                COL_MODEL + " VARCHAR(50)," +
                COL_PLATE + " VARCHAR(50)," +
                COL_FUEL + " VARCHAR(50)," +
                COL_IMG + " VARCHAR(200)," +
                COL_KM + " INTEGER," +
                COL_AC_KM + " INTEGER)"
        val createTable2 = "CREATE TABLE " + TABLE_NAME_2 + " (" +
                COL_ID_PRI + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_ID2 + " VARCHAR(50)," +
                COL_DATE + " VARCHAR(50)," +
                COL_LITER + " INTEGER," +
                COL_KMS + " INTEGER," +
                COL_COST + " INTEGER," +
                "FOREIGN KEY (" + COL_ID2 + ") REFERENCES " + TABLE_NAME_1 + "(" + COL_ID + "))"
        db?.execSQL(createTable)
        db?.execSQL(createTable2)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    // Egy Car példányt tesz az adatbázisba
    fun insertCarData(car: Car) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_ID, car.Nickname)
        cv.put(COL_BRAND, car.brand)
        cv.put(COL_MODEL, car.model)
        cv.put(COL_PLATE, car.plate)
        cv.put(COL_FUEL, car.fuel)
        cv.put(COL_IMG, car.image)
        cv.put(COL_KM, car.kmfirst)
        cv.put(COL_AC_KM, car.actualKM)

        var result = db.insert(TABLE_NAME_1, null, cv)
        if (result == -1.toLong()) {
            Toast.makeText(context, R.string.failedToAddDatabase, Toast.LENGTH_SHORT).show()

        } else {
            //Toast.makeText(context,"Success!", Toast.LENGTH_SHORT).show()
        }
    }

    //Egy Entry példányt tesz az adatbázisba
    fun insertEntryData(entry: Entry, car: Car) {
        val db = this.writableDatabase

        var cv = ContentValues()
        cv.put(COL_ID2, car.Nickname)
        cv.put(COL_DATE, entry.date)
        cv.put(COL_LITER, entry.liter)
        cv.put(COL_KMS, entry.km)
        cv.put(COL_COST, entry.cost)
        var result = db.insert(TABLE_NAME_2, null, cv)

        if (result == -1.toLong()) {
            Toast.makeText(context, R.string.failedToAddDatabase, Toast.LENGTH_SHORT).show()

        } else {
            // Toast.makeText(context,"Success!", Toast.LENGTH_SHORT).show()
        }


    }

    //Kiszedi az összes Car példányt egy ArrayList<Car>-be az adatbázisból.
    fun readCarsData(): ArrayList<Car> {

        var carList = ArrayList<Car>()

        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME_1
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                var oneCar = Car(result.getString(result.getColumnIndex(COL_ID)),
                        result.getString(result.getColumnIndex(COL_BRAND)),
                        result.getString(result.getColumnIndex(COL_MODEL)),
                        result.getString(result.getColumnIndex(COL_PLATE)),
                        result.getString(result.getColumnIndex(COL_FUEL)),
                        result.getString(result.getColumnIndex(COL_KM)).toInt(),
                        result.getString(result.getColumnIndex(COL_AC_KM)).toInt(),
                        result.getString(result.getColumnIndex(COL_IMG)))
                carList.add(oneCar)
            } while (result.moveToNext())
        }

        result.close()
        db.close()

        return carList
    }

    //Selecting Entries by carNick.
    fun readEntryData(carNick: String): ArrayList<Entry> {
        var entryList = ArrayList<Entry>()
        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME_2
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                var oneEntry = Entry(
                        result.getString(result.getColumnIndex(COL_ID2)),
                        result.getString(result.getColumnIndex(COL_DATE)),
                        result.getString(result.getColumnIndex(COL_LITER)).toInt(),
                        result.getString(result.getColumnIndex(COL_KMS)).toInt(),
                        result.getString(result.getColumnIndex(COL_COST)).toInt()
                )
                if (oneEntry.carNick == carNick) {
                    entryList.add(oneEntry)
                }

            } while (result.moveToNext())
        }
        result.close()
        db.close()
        return entryList
    }

    fun readAllEntriesByCar(carnick: String): ArrayList<Entry> {
        var entries = ArrayList<Entry>()
        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME_2
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                var oneEntry = Entry(
                        result.getString(result.getColumnIndex(COL_ID2)),
                        result.getString(result.getColumnIndex(COL_DATE)),
                        result.getString(result.getColumnIndex(COL_LITER)).toInt(),
                        result.getString(result.getColumnIndex(COL_KMS)).toInt(),
                        result.getString(result.getColumnIndex(COL_COST)).toInt()
                )
                if (oneEntry.carNick == carnick) {
                    entries.add(oneEntry)
                }
            } while (result.moveToNext())
        }
        return entries
    }

    //Select ALL Entry
    fun readAllEntryData(): ArrayList<Entry> {
        var entryList = ArrayList<Entry>()
        val db = this.readableDatabase

        val query = "Select * from " + TABLE_NAME_2
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                var oneEntry = Entry(
                        result.getString(result.getColumnIndex(COL_ID2)),
                        result.getString(result.getColumnIndex(COL_DATE)),
                        result.getString(result.getColumnIndex(COL_LITER)).toInt(),
                        result.getString(result.getColumnIndex(COL_KMS)).toInt(),
                        result.getString(result.getColumnIndex(COL_COST)).toInt()
                )
                entryList.add(oneEntry)
            } while (result.moveToNext())
        }
        result.close()
        db.close()
        return entryList
    }

    fun upDateKms(carNick: String, newKms: Int) {
        val db = this.writableDatabase
        val query = "UPDATE " + TABLE_NAME_1 + " SET " + COL_AC_KM +
                " = " + newKms + " WHERE " + COL_ID + " = '" + carNick + "'"

        db.execSQL(query)
        db.close()
    }

    fun deleteAllEntryForDeleteCar(car: Car) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME_2, COL_ID2 + "=?", arrayOf((car.Nickname).toString()))

    }

    fun deleteCarItem(car: Car) {

        val picturePath = getCarPicturePath(car.Nickname)
        if (picturePath != null && picturePath!!.length != 0) {
            val carFilePath = File(picturePath)
            carFilePath.delete()
        }
        val db = this.writableDatabase

        // db.delete(TABLE_NAME_2,COL_ID2+"=?", arrayOf(car.Nickname.toString() ))
        val result = db.delete(TABLE_NAME_1, COL_ID + "=?", arrayOf((car.Nickname).toString()))


        if (result > 0) {
            Toast.makeText(context, R.string.deleted, Toast.LENGTH_SHORT).show()
        }
    }

    //egy autóhoz tartozó kép visszakapása
    fun getCarPicture(carId: String): Bitmap? {
        val picturePath = getCarPicturePath(carId)
        return if (picturePath == null || picturePath!!.isEmpty()) null else BitmapFactory.decodeFile(picturePath)

    }


    fun CarPictureSet(picName: String, picture: Bitmap) :String {
        // Elmenti a képet a belső mappába saját egyedi azonosítóval
        var picturePath = ""
        val internalStorage = this.context.getDir("CarPictures", Context.MODE_PRIVATE)
        val carFilePath = File(internalStorage, "$picName.png")
        picturePath = carFilePath.toString()

        var fos = FileOutputStream(carFilePath)
        picture.compress(Bitmap.CompressFormat.PNG, 100 , fos)
        fos.close()

        val newPictureValue = ContentValues()
        newPictureValue.put(COL_IMG, picturePath)


        return picturePath

    }
    private fun getCarPicturePath(carId: String): String {

        val db = readableDatabase

        val carCursor = db.query(TABLE_NAME_1,
                null,
                COL_ID + "=?",
                arrayOf(carId.toString()), null, null, null)
        carCursor.moveToNext()

        return carCursor.getString(carCursor.getColumnIndex(COL_IMG))
    }

}
