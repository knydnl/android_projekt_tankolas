package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_add_cars.*
import kotlinx.android.synthetic.main.activity_entry_detail.*


class EntryDetailActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.titleOneEntry)
        setContentView(R.layout.activity_entry_detail)
        var logic = Logic(this)
        var carName: String = intent.extras.getString("carNick").toString()
        var carKm: Int = intent.extras.getInt("kms")
        var actualCar = logic.SearchCarByNick(carName)
        var entry = logic.searchEntry(carName, carKm)
        var fuelUnit = ""
        var fuelUnitWoutKM = ""
        if (actualCar.fuel == FuelType.Petrol.toString() || actualCar.fuel == FuelType.Diesel.toString() || actualCar.fuel == FuelType.Gas.toString()) {
            fuelUnit = " liter/100km"
            fuelUnitWoutKM = " liter"
        }
        if (actualCar.fuel == FuelType.Electricity.toString()) {
            fuelUnit = " kWh/100km"
            fuelUnitWoutKM = " kWh"
        }
        var kmsInThisSession = logic.KmsInThisSession(actualCar, entry)
        // x/100km
        var averageConsumption = (entry.liter.toDouble() / kmsInThisSession.toDouble()) * 100

        var consTextString = String.format("%.2f", averageConsumption)
        consTextString += fuelUnit
        dateOfFueling.text = entry.date
        currentCar.text = entry.carNick
        FuelInLiters.text = entry.liter.toString() + fuelUnitWoutKM
        priceOfRefueling.text = entry.cost.toString() + " Ft"
        kmsText.text = entry.km.toString() + " Km"
        kmDone_text.text = kmsInThisSession.toString() + " Km"
        consumption_text.text = consTextString
        totalcost_text.text = entry.totalCost.toString()


    }

}
