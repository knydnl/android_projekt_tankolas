package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.app.PendingIntent.getActivity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import hu.uniobuda.nik.tankolas.tankolasnaplo.R.id.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_mainsite.*
import kotlinx.android.synthetic.main.fragment_statistics.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var frag:Fragment
    var logic = Logic(this)

    var cars = ArrayList<Car>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        logic.ReadCarsData()
        this.cars=logic.Cars


        loadMainsite(MainsiteFragment())


//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
//        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

//        AlertDialog.Builder(this)
//
//                .setView(layoutInflater.inflate(R.layout.dialog_addname, null))
//                .setPositiveButton("OK") { dialog, which ->
//                    Toast.makeText(applicationContext, "Sikeres mentés!", Toast.LENGTH_SHORT).show()
//
//
//                }.show() ?: throw IllegalStateException("Meg kell adnia egy felhasználónevet!")

    }

    override fun onStart() {
        super.onStart()
        if(this.cars.isEmpty()){
            chooseonecar.visibility= View.INVISIBLE
            main_recyclerview.visibility=View.INVISIBLE
            AlertDialog.Builder(this)
                    .setMessage(R.string.noCar)
                    .setTitle(R.string.noCarTitle)
                    .setPositiveButton(R.string.yes) { dialog, which ->
                        val intent = Intent(this, AddCarsActivity::class.java)
                        startActivity(intent)
                    }
                    .setNegativeButton(R.string.no) { dialog, which ->
                    }.show()
        }
    }
    override fun onResume() {
        super.onResume()
        if(this.cars.isEmpty()){
            chooseonecar.visibility= View.INVISIBLE
            main_recyclerview.visibility=View.INVISIBLE
            AlertDialog.Builder(this)
                    .setMessage(R.string.noCar)
                    .setTitle(R.string.noCarTitle)
                    .setPositiveButton(R.string.yes) { dialog, which ->
                        val intent = Intent(this, AddCarsActivity::class.java)
                        startActivity(intent)
                    }
                    .setNegativeButton(R.string.no) { dialog, which ->
                    }.show()
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if(frag is MainsiteFragment){
                super.onBackPressed()

            }
            else{
                loadMainsite(MainsiteFragment())
            }
        }
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_entry -> {
                setTitle(R.string.titleEntries);
                loadEntry(EntryFragment())
            }
            R.id.nav_myCars -> {
                setTitle(R.string.titleMyCars);
                loadMyCars(MyCarsFragment())
            }
            R.id.nav_statistics -> {
                setTitle(R.string.titleStatistic);
                loadStatistics(StatisticsFragment())
            }
            R.id.nav_info -> {
                setTitle(R.string.titleInformation);
                loadInfo(InfoFragment())
            }


        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    //region LoadFragments
    private fun loadInfo(fragment: InfoFragment) {
        val fm = supportFragmentManager.beginTransaction()
        fm.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        fm.replace(R.id.frameLayout, fragment)
        fm.commit()
        frag=  InfoFragment()
    }

    private fun loadEdit(fragment: EditFragment) {
        val fm = supportFragmentManager.beginTransaction()
        fm.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        //fm.addToBackStack(EditFragment::class.java.name)
        fm.replace(R.id.frameLayout, fragment)
        fm.commit()
        frag=  EditFragment()
    }

    private fun loadMainsite(fragment: MainsiteFragment) {
        val fm = supportFragmentManager.beginTransaction()
        fm.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        //fm.addToBackStack(MainsiteFragment::class.java.name)
        val fragment2 = MainsiteFragment.newInstance(this.logic)
        fm.replace(R.id.frameLayout, fragment2)
        fm.commit()

        frag = MainsiteFragment()

    }

    private fun loadStatistics(fragment: StatisticsFragment) {
        val fm = supportFragmentManager.beginTransaction()
        fm.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        val fragment2 = StatisticsFragment.newInstance(this.logic)

        fm.replace(R.id.frameLayout, fragment2)
        fm.commit()
        frag=  StatisticsFragment()
    }

    private fun loadMyCars(fragment: MyCarsFragment) {
        val fm = supportFragmentManager.beginTransaction()
        fm.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        val fragment2 = MyCarsFragment.newInstance(this.logic)

        fm.replace(R.id.frameLayout, fragment2)
        fm.commit()
        frag=  MyCarsFragment()

    }

    private fun loadEntry(fragment: EntryFragment) {
        val fm = supportFragmentManager.beginTransaction()
        fm.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
       // fm.addToBackStack(EntryFragment::class.java.name)
        val fragment2 = EntryFragment.newInstance(this.logic)

        fm.replace(R.id.frameLayout, fragment2)
        fm.commit()
        frag=  EntryFragment()
    }
    //endregion
}
