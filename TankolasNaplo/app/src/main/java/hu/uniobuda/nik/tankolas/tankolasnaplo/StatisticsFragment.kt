package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_statistics.*
import kotlin.collections.ArrayList

//region View texts
var FuelUnit=""
//endregion
class StatisticsFragment : Fragment() {

    private var context = MainActivity()
    private var mylogic = Logic(context)
    private var mycars = ArrayList<Car>()

    companion object {
        fun newInstance(logic: Logic): StatisticsFragment {

            val fragment = StatisticsFragment()
            fragment.mylogic = logic
            fragment.mylogic.ReadCarsData()
            fragment.mycars = logic.Cars
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val spinner = carslider_stat
        mylogic.ReadCarsData()
        var options = ArrayList<String>()
        options.add("Válasszon!")
        for (car in mylogic.Cars) {
            if (car.entryList.isNotEmpty()) {
                options.add(car.Nickname)
            }
        }

        val noDataOptions = ArrayList<String>()
        if (options.count() <= 1) {

            spinner.adapter = ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, noDataOptions)
        } else {
            spinner.adapter = ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, options)
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (options.count() > 1 && position != 0) {
                    var seged = options[position]
                    var Car = mylogic.SearchCarByNick(seged)

                    if (Car.fuel == FuelType.Petrol.toString() || Car.fuel == FuelType.Diesel.toString() || Car.fuel == FuelType.Gas.toString()){
                        FuelUnit=" liter/100km"
                    }
                    if (Car.fuel == FuelType.Electricity.toString()){
                        FuelUnit=" kWh/100km"
                    }
                    var avgtext = String.format("%.2f",mylogic.AverageConsumptionAtAll(Car))
                    avgtext += " $FuelUnit"
                    avg_km_text.text = avgtext
                    avg_cost_km_text.text = mylogic.AverageCostPerKmAtAll(Car).toString() + " Ft/Km"
                    var entry = mylogic.MostExpensiveEntry(Car.entryList)
                    var mostCostString = "Dátum: " + entry.date + " |Összár: " + entry.totalCost.toString() + " Ft " +
                            "\n |Tankolt mennyiség: " + entry.liter.toString()+" "+ FuelUnit + " |Egységára: " + entry.cost.toString() +" Ft"
                    most_cost_entry_text.text = mostCostString
                    all_cost_text.text = mylogic.AllCost(Car).toString()+" Ft"
                    km_text.text = Car.actualKM.toString() + " Km"
                }
                else {
                    avg_cost_km_text.text=""
                    avg_km_text.text=""
                    most_cost_entry_text.text=""
                    all_cost_text.text=""
                    km_text.text=""
                }

            }

        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(container?.context).inflate(R.layout.fragment_statistics, container, false)
    }

}