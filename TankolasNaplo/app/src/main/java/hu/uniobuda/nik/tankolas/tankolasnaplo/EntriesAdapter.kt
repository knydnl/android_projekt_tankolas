package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.security.KeyStore


class EntriesAdapter(val context: Context, val entries: ArrayList<Entry>) : RecyclerView.Adapter<EntryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view=inflater.inflate(R.layout.entrylistitem, parent, false)
        return EntryViewHolder(view)
    }

    //inlined function
    override fun getItemCount()= entries.size


    override fun onBindViewHolder(viewHolder: EntryViewHolder, position: Int) {
        var logic= Logic(context)

        var fuelUnit=""
        val item :Entry= entries[position]
        var car = logic.SearchCarByNick(item.carNick)
        if (car.fuel == FuelType.Electricity.toString()){
            fuelUnit=" kWh"
        }
        else{
            fuelUnit=" liter"
        }
        viewHolder?.name?.text=item.carNick + " - "
        viewHolder?.date?.text=item.date
        viewHolder?.liter?.text=item.liter.toString() + fuelUnit
        viewHolder?.km?.text=item.km.toString() + " km"
        viewHolder.itemView.setOnClickListener {
            val intent = Intent(this.context, EntryDetailActivity::class.java)
            intent.putExtra("carNick", item.carNick)
            intent.putExtra("kms",item.km)
            context.startActivity(intent)
        }


    }


}

class EntryViewHolder(view: View) : RecyclerView.ViewHolder(view){
    val name: TextView = view.findViewById(R.id.name)
    val date : TextView = view.findViewById(R.id.date)
    val liter: TextView =view.findViewById(R.id.liter)
    val km: TextView =view.findViewById((R.id.km)
    )
}