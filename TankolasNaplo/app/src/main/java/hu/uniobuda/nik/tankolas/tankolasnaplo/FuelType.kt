package hu.uniobuda.nik.tankolas.tankolasnaplo

enum class FuelType {
    Petrol,
    Diesel,
    Gas,
    Electricity;

    override fun toString(): String {
        if (this == FuelType.Gas)
        {
            return "Gáz"
        }
        if (this == FuelType.Petrol)
        {
            return "Benzin"
        }
        if (this == FuelType.Electricity)
        {
            return "Áram"
        }
        if (this == FuelType.Diesel)
        {
            return "Dízel"
        }
        else{
            return ""
        }
    }
}