package hu.uniobuda.nik.tankolas.tankolasnaplo


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_mainsite.*

class MainsiteFragment: Fragment() {

    private var cars = ArrayList<Car>()
    private lateinit var mylogic : Logic
    private var adapter : MainCarsAdapter? = null


    companion object {
        fun newInstance(logic: Logic): MainsiteFragment{


            val fragment = MainsiteFragment()
            fragment.mylogic = logic
            fragment.mylogic.ReadCarsData()
            fragment.cars=fragment.mylogic.Cars
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(container?.context).inflate(R.layout.fragment_mainsite, container, false)

    }

    override fun onResume() {
        super.onResume()
        mylogic.ReadCarsData()
        cars=mylogic.Cars
        adapter=null
        updateUI()
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        updateUI()
    }
    private fun updateUI(){
        if(adapter==null){
            adapter= MainCarsAdapter(requireContext(),cars)
            main_recyclerview.adapter=adapter
            main_recyclerview.layoutManager= LinearLayoutManager(this.context, OrientationHelper.HORIZONTAL, false)
        }
        adapter?.notifyDataSetChanged()
    }
}