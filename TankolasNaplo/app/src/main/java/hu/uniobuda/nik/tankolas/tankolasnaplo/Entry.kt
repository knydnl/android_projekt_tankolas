package hu.uniobuda.nik.tankolas.tankolasnaplo

import java.util.*

data class Entry (val carNick: String,
                  val date:String,
                  val liter:Int, // liters refueled
                  val km:Int,
                  val cost:Int  // Forint/Liters
){
    var totalCost = liter*cost  //Total cost of the refueling
}
