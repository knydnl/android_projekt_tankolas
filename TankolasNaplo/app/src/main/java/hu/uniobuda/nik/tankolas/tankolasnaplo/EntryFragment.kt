package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_entry.*
import java.util.*

class EntryFragment: Fragment(){

    private var entries = ArrayList<Entry>()
    private lateinit var mylogic : Logic
    private var adapter : EntriesAdapter? = null
    private var checkedList =ArrayList<Boolean>()
    private var carList = ArrayList<String>()
    private lateinit var carsArray : Array<String>

    companion object {
        fun newInstance(logic: Logic): EntryFragment{
            val fragment =EntryFragment()
            fragment.mylogic=logic
            fragment.mylogic.ReadCarsData()
            fragment.entries = fragment.mylogic.readAllEntry()


            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return LayoutInflater.from(container?.context).inflate(R.layout.fragment_entry, container, false)
    }

    override fun onResume() {
        super.onResume()
        this.mylogic.ReadCarsData()
        this.entries=mylogic.readAllEntry()
        adapter=null
        updateUI()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.mylogic.ReadCarsData()
        this.entries = mylogic.readAllEntry()


        for(car in mylogic.Cars)
        {
            carList.add(car.Nickname)
            checkedList.add(false)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addentry.setOnClickListener { view ->
            startAddEntryActivity()
        }
        //if car was founc
        chooseCar.setOnClickListener { view ->
            if(carList.isNotEmpty()){
                chooseCarFromList()
            }
            //if no cars were found
            else{
                AlertDialog.Builder(this.context!!)
                        .setMessage(R.string.noCar)
                        .setTitle(R.string.noCarTitle)
                        .setPositiveButton(R.string.yes) { dialog, which ->
                            startAddCarActivity()
                        }
                        .setNegativeButton(R.string.no) { dialog, which ->
                        }.show()
            }

        }

        updateUI()
    }


    fun startAddEntryActivity(){
        val intent = Intent(this.context, AddEntryActivity::class.java);
        startActivity(intent)
    }

    fun startAddCarActivity(){
        val intent = Intent(this.context, AddCarsActivity::class.java);
        startActivity(intent)
    }



    fun chooseCarFromList(){
        var checkedCars = BooleanArray(carList.size) { i-> false}
        carsArray= Array(carList.size) { i -> carList[i]}
        var checkedCarsName = ArrayList<String>()

        val builder = AlertDialog.Builder(this.context!!)
        builder.setTitle(R.string.chooseCar)
        builder.setMultiChoiceItems(carsArray, checkedCars){dialog, which, isChecked ->
            checkedCars[which] = isChecked

        }


        builder.setPositiveButton("OK"){dialog, which ->

            for ((idx, items) in checkedCars.withIndex()){
                if (items){
                    checkedCarsName.add(carList[idx])
                }
            }
            entries=this.mylogic.readSelectedEntry(checkedCarsName)
            adapter=null
            updateUI()

        }
      
    builder.setNeutralButton(R.string.showAll){dialog, which ->

        checkedCars = BooleanArray(carList.size, {i-> true})
        entries=mylogic.readAllEntry()
        adapter=null
        updateUI()


   }
       builder.setNegativeButton(R.string.no){dialog, which ->  }

        builder.show()

    }


    private fun updateUI(){
        if(adapter==null){
            adapter= EntriesAdapter(requireContext(),entries)
            entries_recyclerview.adapter=adapter
            entries_recyclerview.layoutManager= LinearLayoutManager(this.context)
        }
        adapter?.notifyDataSetChanged()
    }

}