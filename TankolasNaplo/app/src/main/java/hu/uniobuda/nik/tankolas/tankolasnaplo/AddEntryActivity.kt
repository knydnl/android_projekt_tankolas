package hu.uniobuda.nik.tankolas.tankolasnaplo


import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.Editable
import android.text.TextWatcher
import android.widget.Spinner
import android.view.View
import android.widget.*
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_entry.*
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

class AddEntryActivity : AppCompatActivity() {

    private lateinit var option: Spinner
    private var actualKmCheck = 0
    val context = this
    var logic = Logic(context)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.titleOneEntry)
        setContentView(R.layout.activity_add_entry)


        logic.ReadCarsData()
        val options = ArrayList<String>()
        options.add("Válasszon!")




        for (car in logic.Cars) {
            options.add(car.Nickname)
        }
        val noDataOptions = ArrayList<String>()

        this.option = findViewById(R.id.carslider)
        if (options.count() <= 1) {
            this.option.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, noDataOptions)
        } else {
            this.option.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options)
        }

        //Set "carslider" and KMs if opened from mainPage
        if (intent.hasExtra("NickName")) {
            var carName = intent.extras.getString("NickName").toString()
            if (carName.isNotEmpty()) {
                var passedCar = logic.SearchCarByNick(carName)
                addKms.setText(passedCar.actualKM.toString())
                carslider.setSelection(getPosition(options, carName))
            }
        }
        val seekbar = findViewById<View>(R.id.fuelliter) as SeekBar
        val value = findViewById<TextView>(R.id.seekbarValue)
        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                value.text = "$progress"

            }

        })
        seekbarValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if (seekbarValue.text.toString().isNotEmpty()) {

                    var valid = true
                    for (char in seekbarValue.text.toString()) {
                        if (!char.isDigit()) {
                            valid = false
                        }
                    }
                    if (!valid) {
                        seekbarValue.setText("0")
                        Toast.makeText(context, R.string.onlyNumbers, Toast.LENGTH_SHORT).show()
                    }
                    seekbar.progress = seekbarValue.text.toString().toInt()
                    if (seekbarValue.text.toString().toInt() > 120) {
                        seekbarValue.setText(120.toString())
                    }
                } else {
                    seekbar.progress = 0
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        saveEntry.setOnClickListener {
            if (carslider.selectedItem != null &&
                    seekbarValue.text.toString().toInt() > 0 &&
                    priceinput.text.toString().isNotEmpty() &&
                    dateChoose.text.toString().isNotEmpty() &&
                    addKms.text.toString().isNotEmpty() &&
                    carslider.selectedItem.toString() != "Válasszon!") {


                if (addKms.text.toString().toInt() <= actualKmCheck) {
                    Toast.makeText(context, R.string.kmWrongData, Toast.LENGTH_SHORT).show()
                    addKms.setText(actualKmCheck.toString())
                } else {
                    var validEntry = Entry(carslider.selectedItem.toString(),
                            dateChoose.text.toString(),
                            seekbarValue.text.toString().toInt(),
                            addKms.text.toString().toInt(),
                            priceinput.text.toString().toInt())
                    logic.AddToEntryData(validEntry, logic.SearchCarByNick(carslider.selectedItem.toString()))
                    Toast.makeText(context, R.string.addSuccess, Toast.LENGTH_SHORT).show()
                    val intent = Intent(context, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

        }

        this.option.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (options.count() > 1 && position != 0) {
                    var seged = options.get(position)
                    actualKmCheck = logic.SearchCarByNick(seged).actualKM
                    var carKM = logic.SearchCarByNick(seged).actualKM.toString()
                    addKms.setText(carKM)
                }
            }

        }
    }


    fun funDate(view: View) {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)


        val dpd = DatePickerDialog(this, android.R.style.Theme_Material_Light_Dialog, DatePickerDialog.OnDateSetListener { datePicker, year, monthOfYear, dayOfMonth ->
            dateChoose.text = ("" + year + "." + (monthOfYear + 1) + "." + dayOfMonth + ".")
        }, year, month, day)
        dpd.datePicker.maxDate = (System.currentTimeMillis())
        dpd.show()
    }

    fun getPosition(list: ArrayList<String>, name: String): Int {
        var counter = 0
        var pos = 0
        for (names in list) {
            if (names == name) {
                pos = counter
            }
            counter++
        }
        return pos
    }


    //region Validator functions
    /*private fun isKmValid(km: String): Boolean {
        return if (km.toInt()<=999999999) true
        else{
            Toast.makeText(context,"A maximálisan megadható kilométeróra állása: 999999999!",Toast.LENGTH_SHORT).show()
            false
        }
    }

    private fun isCostValid(cost: String): Boolean {
        return if (cost.toInt()<=999) true
        else{
            Toast.makeText(context,"A maximálisan megadható egységár: 999!",Toast.LENGTH_SHORT).show()
            false
        }
    } */
    //endregion
}
