package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class InfoFragment: Fragment(){


    companion object {
        fun newInstance(): InfoFragment{

            return  newInstance()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(container?.context).inflate(R.layout.fragment_info, container, false)


    }
}