package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.hdodenhof.circleimageview.CircleImageView

class MainCarsAdapter(val context: Context, val mycars: ArrayList<Car>) : RecyclerView.Adapter<MainCarViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainCarViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view=inflater.inflate(R.layout.carpicturelistitem, parent, false)
        return MainCarViewHolder(view)
    }

    //inlined function
    override fun getItemCount()= mycars.size


    override fun onBindViewHolder(viewHolder: MainCarViewHolder, position: Int) {
        val item : Car= mycars[position]
        viewHolder?.name?.text=item.Nickname
        if (item.image=="default"){
            viewHolder?.image?.setImageResource(R.drawable.car)
        }
        else{
            viewHolder?.image?.setImageBitmap(BitmapFactory.decodeFile(item.image))
        }

        viewHolder.itemView.setOnClickListener {
            val intent = Intent(this.context, AddEntryActivity::class.java)
            intent.putExtra("NickName", item.Nickname)
            context.startActivity(intent)
        }

    }

}

class MainCarViewHolder(view: View) : RecyclerView.ViewHolder(view){
    val name : TextView = view.findViewById(R.id.mainname)
    val image: CircleImageView=view.findViewById(R.id.maincircle_auto)
}