package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.hdodenhof.circleimageview.CircleImageView
import hu.uniobuda.nik.tankolas.tankolasnaplo.R.id.imgviewcar
import kotlinx.android.synthetic.main.activity_car_detail.*

class CarsAdapter(val context: Context, val mycars: ArrayList<Car>) : RecyclerView.Adapter<CarViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view=inflater.inflate(R.layout.carlistitem, parent, false)
        return CarViewHolder(view)
    }

    //inlined function
    override fun getItemCount()= mycars.size


    override fun onBindViewHolder(viewHolder: CarViewHolder, position: Int) {
        val item : Car= mycars[position]
        viewHolder.nickname.text=item.Nickname
        viewHolder.plate.text=item.plate
        viewHolder.brand.text=item.brand
        viewHolder.model.text=item.model
        if (item.image=="default"){
            viewHolder.image.setImageResource(R.drawable.car)
        }
        else{
            viewHolder.image.setImageBitmap(BitmapFactory.decodeFile(item.image))
        }

        //Activity starting, passing a string (carname) to it.
        viewHolder.itemView.setOnClickListener {
            val intent = Intent(this.context, CarDetailActivity::class.java)
            intent.putExtra("carNick",item.Nickname)
            context.startActivity(intent)
        }


    }


}

class CarViewHolder(view: View) : RecyclerView.ViewHolder(view){
    val nickname : TextView = view.findViewById(R.id.nickname)
    val plate: TextView =view.findViewById(R.id.plate)
    val brand: TextView =view.findViewById(R.id.brand)
    val model: TextView =view.findViewById((R.id.model)
    )
    val image: CircleImageView=view.findViewById(R.id.circle_auto)
}