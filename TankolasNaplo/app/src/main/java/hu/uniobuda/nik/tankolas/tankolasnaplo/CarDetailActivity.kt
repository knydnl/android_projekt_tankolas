package hu.uniobuda.nik.tankolas.tankolasnaplo

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.opengl.Visibility
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import kotlinx.android.synthetic.main.activity_car_detail.*

class CarDetailActivity : AppCompatActivity() {


    //Object for binding the values
    private lateinit var selectedCar: Car
    val logic = Logic(this)
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.titleOneCar)
        setContentView(R.layout.activity_car_detail)

        //getting the passed value(carnick) and with logic.SearchCarByNick getting the car object for binding the datas
        var carName: String = intent.extras.getString("carNick").toString()
        var fuelUnit = ""

        selectedCar = logic.SearchCarByNick(carName)
        if (selectedCar.fuel == FuelType.Electricity.toString()) {
            fuelUnit = " kWh/100km"
        } else {
            fuelUnit = " liter/100km"
        }


        if (selectedCar.image=="default"){
            imgviewcar.setImageResource(R.drawable.car)
        }
        else{
            imgviewcar.setImageBitmap(BitmapFactory.decodeFile(selectedCar.image))
        }
        car_name.text = selectedCar.Nickname
        car_plate.text = selectedCar.plate
        car_brand.text = selectedCar.brand
        car_fuel.text = selectedCar.fuel
        car_model.text = selectedCar.model
        car_fuel.text = selectedCar.fuel
        car_kmoraallas.text = selectedCar.actualKM.toString() + " km"


        if (selectedCar.entryList.isNotEmpty()) {
            var avgString = String.format("%.2f", logic.AverageConsumptionAtAll(selectedCar))
            car_atlagfogyasztas.text = avgString + fuelUnit
            car_legutobbi.text = selectedCar.entryList.last().date
        } else {
            legutobbi.visibility= View.INVISIBLE
            atlagfogyasztas.visibility=View.INVISIBLE
            car_atlagfogyasztas.visibility=View.INVISIBLE
            car_legutobbi.visibility=View.INVISIBLE
        }



        deletecar_button.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.delete)
            builder.setMessage(R.string.sureDelete)
            builder.setPositiveButton(R.string.yes, { dialog: DialogInterface?, which: Int -> ifDelete() })
            builder.setNegativeButton(R.string.no, { dialog: DialogInterface?, which: Int -> })
            builder.show()

        }
    }

    fun ifDelete() {
        logic.DeleteCar(selectedCar)
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

}
